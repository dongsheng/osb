MSBuild libsqlite3.vcxproj /t:Rebuild /p:PlatformToolset=v110;Configuration=Release;Platform=Win32
MSBuild libsqlite3.vcxproj /t:Rebuild /p:PlatformToolset=v110;Configuration=Release;Platform=x64

MSBuild sqlite3.vcxproj /t:Rebuild /p:PlatformToolset=v110;Configuration=Release;Platform=Win32
MSBuild sqlite3.vcxproj /t:Rebuild /p:PlatformToolset=v110;Configuration=Release;Platform=x64

gcc -O2 -s -o sqlite3 \
    -DHAVE_READLINE=1 \
    -DSQLITE_DEFAULT_MEMSTATUS=0 \
    -DSQLITE_DEFAULT_WAL_SYNCHRONOUS=1 \
    -DSQLITE_ENABLE_COLUMN_METADATA=1 \
    -DSQLITE_ENABLE_DBSTAT_VTAB=1 \
    -DSQLITE_ENABLE_FTS3=1 \
    -DSQLITE_ENABLE_FTS4=1 \
    -DSQLITE_ENABLE_FTS5=1 \
    -DSQLITE_ENABLE_JSON1=1 \
    -DSQLITE_ENABLE_LOAD_EXTENSION=1 \
    -DSQLITE_ENABLE_RBU=1 \
    -DSQLITE_ENABLE_RTREE=1 \
    -DSQLITE_ENABLE_UNLOCK_NOTIFY=1 \
    -DSQLITE_LIKE_DOESNT_MATCH_BLOBS=1 \
    -DSQLITE_MAX_EXPR_DEPTH=0 \
    -DSQLITE_OMIT_DEPRECATED=1 \
    -DSQLITE_OMIT_PROGRESS_CALLBACK=1 \
    -DSQLITE_OMIT_SHARED_CACHE=1 \
    -DSQLITE_SECURE_DELETE=1 \
    shell.c sqlite3.c -lreadline -lm -lpthread -ldl

ldd `which sqlite3`
ldd sqlite3
